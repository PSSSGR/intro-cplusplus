//FILENAME   :PROGRAM4
//NAME       :SRI PADALA
//WSU ID     :U424P963
//HW         :HOMW WORK-4
//DESCRIPTION:THIS PROGRAM WILL PROMPT THE USER TO SELECT HIS/HER SNACK ITEM FROM VENDING MACHINE AND MAKES CALCULATIONS REGARDING MONEY TRANSACTIONS INVOLVED AND WILL DISPLAY THE EXTRA AMOUNT IN LEAST NUMBER OF COINS.
#include <iostream>
#include <iomanip>//To set width.
#include <cctype>//To convert lower case letter to upper case letter.

using namespace std;

int menu (void);//It shows up menu and prompts the user to make selection.
int acceptmoney ( int price );//It takes money from user and adds to total.
int compute_change ( int temp_total,int price );//Computes change and returns it to user.
int numb_coins( int change );//Shows the dispencing amount in least number of coins.

//Starting out the main function.
int main(void)

{
//int total_price;
char choice;

while(1)//Repeats the main function after each purchase.
	{
		menu();
		cout << "would yo care to make another purchase (Y/N):  ";
		cin  >> choice;
		cout << endl;
		choice = toupper(choice);

		if( choice == 'N' )
		{
		cout << "Thank you and enjoy your purchase!" << endl;
		break;
		}
		cout << endl;
	}
	return(0);
}

//The menu function.
int menu (void)
{

char choice;
int temp_total,change;
		cout << "Welcome to snack machine: \n";
		cout << "Available snack to select from : \n"<< "P - Potato Chips"<<setw(12)<<" $1.25 \n";
		cout << "S - Snickers"<<setw(15)<<"$0.35\n"<<"T - Pop Tart"<<setw(15)<<" $0.95\n";
		cout << "C - Cookies"<<setw(16)<<"$1.50\n"<<"B - Brownie"<<setw(16)<<" $1.75\n"<<"N - Nuts"<<setw(20)<<"$1.40 \n";
		cout << "Please enter the letter labeling your snack selection:  ";
		cin  >> choice;

		switch(choice)
		{
		 case 'p':
		 case 'P':
	     cout << "Your selected costs :125 cents ";
		 temp_total = acceptmoney(125);//
		 change = compute_change(temp_total,125);//Values will be sent from here to other functions and gets returned.
		 numb_coins(change);//
		 break;
	     case 's':
		 case 'S':
		 cout << "Your selected costs : 35 cents";
		 temp_total = acceptmoney(35);
		 change = compute_change(temp_total,35);
		 numb_coins(change);
		 break;
		 case 't':
		 case 'T':
		 cout << "Your selected costs : 95 cents";
		 temp_total = acceptmoney(95);
		 change = compute_change(temp_total,95);
		 numb_coins(change);
		 break;
		 case 'c':
		 case 'C':
		 cout << "Your selected costs : 150 cents";
		 temp_total = acceptmoney(150);
		 change = compute_change(temp_total,150);
		 numb_coins(change);
		 break;
		 case 'b':
		 case 'B':
		 cout << "Your selected costs : 175 cents";
		 temp_total = acceptmoney(175);
		 change = compute_change(temp_total,175);
		 numb_coins(change);
		 break;
		 case 'n':
		 case 'N':
		 cout << "Your selected costs : 140 cents";
		 temp_total = acceptmoney(140);
		 change = compute_change(temp_total,140);
		 numb_coins(change);
		 break;
																    
		 default:
		 cout << "Invalid selection!" <<endl;
		}
	return(0);
}
//accept Money
int acceptmoney(int price)
{
	int money_entered=0,temp_total=0;
	char choice;
	cout << "\nMoney accepted by the machine: \n" << "N - Nickel\n" << "Q - Quarter\n" << "D - Dollar\n";
	
	while (temp_total<price)
	{
		cout << "Your selected costs : " << price << endl;
		cout << "You entered money:  " << temp_total << endl;
		cout << "Insert amount (enter letter of choice): ";
		cin  >> choice;

		if( choice == 'N' || choice == 'n')
		{
			money_entered = 5;
			temp_total+= money_entered;//For each iteration an amount of 5 cents will added to temp_total.
		}
		else if( choice == 'Q' || choice == 'q')
		{
			money_entered = 25;
			temp_total+= money_entered;//For each iteration an amount of 25 cents will added to temp_total.
		}
		else if(choice == 'D' || choice == 'd')
		{
			money_entered = 100;
			temp_total+= money_entered;//For each iteration an amount of 100 cents will added to temp_total.
		}
		else 
		{
			cout << choice << "  is not recognised as a coin" << endl;
		}
	}
	return(temp_total);//returns temp_total to original function.
}


//function compute_change
int compute_change(int temp_total,int price)
{
	int change;
	change = temp_total - price ;
	cout << "Your total inserted:  " << temp_total << endl;
	cout << "Dispensing Change : " << change << endl;
	return(change);
}


//function change_coin
int numb_coins(int change)
{
	int i=0,j=0,k=0;//i=number of dollar coins,j=number of quater coins,k=number of nickels.
	

	while( change >= 100 )
	{
		if( change>=100 )
			change-= 100;
		i++;
	}
	while ( change >= 25 )
	{
		if( change>=25 )
			change-= 25;
		j++;
	}
	while( change >= 5 )
	{
		if( change>=5 )
			change-= 5;
		k++;
	}
	cout << "Number of dollars : " << i << endl;
	cout << "Number of quaters : " << j << endl;
	cout << "Number of nickels : " << k << endl;
	
	
	return(0);
}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

