// Filename:program10.cpp
// Name:Sri Padala
// WSUID:U424P963
// HW:home work 10
// Description:this program will convet zipcode to barcode and vice versa.

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

void processZip(int prompt);//To prompt the user.


class PostalService
{
	public:
		PostalService(int codeNumber);//constructor with interger type
		PostalService(string barString);//constructor with string type
		void print_barcode(string barcode,ostream& file);//prints to the screen and to file
	


	private:
		string barCode;//stores the code.
		void findBarCode(int);//converts the codes
		void findZipCode(string);//converts the codes

};


int main()
{
	int mainMenu;
	cout << "This program is able to convert zip codes to a POSTNET format " 
		 << "and vice versa\n" 
		 << "\t1. Convert zip code to POSTNET\n" 
		 << "\t2. Convert POSTNET to zip code\n" 
		 << "\t3. Quit\n";
	
	do {
		cout << "Please make your selection: ";
		cin >> mainMenu;
		switch(mainMenu) {
			case 1: 
			case 2: 
				processZip(mainMenu); //caling the function
				break;
			default:
				if (mainMenu !=3)
					cout << "Invalid choice...\n";
				else 
					cout << "\n";
		} 
	}
	while (mainMenu != 3);
	return 0; 
}
void processZip(int prompt){
	int zip;
	string bar;
	if (prompt == 1){
		cout << "Enter a zip code in roman format (#####):  ";
		cin >> zip;
		cout << endl;
		cout << "Your zip code is " << zip<<", and the bar code looks like this:\n";
		PostalService service1(zip);//calling the constructor.
		
	
	}else if (prompt == 2){
		cout << "Enter a zip code in bar code format (1’s and 0’s):  ";
		cin >> bar;
		cout << endl;
		bar.erase(0,1);//erasing the traling and leading numbers.
		bar.erase(25,1);
		PostalService service2(bar);//calling the constructor.
	}
			
}

PostalService::PostalService(int codeNumber) {//declaring
	int	zipCode = codeNumber;
	barCode = "";
	findBarCode(zipCode);
}


PostalService::PostalService(string barString) {
	barCode = barString;
	findZipCode(barString);
}
void PostalService::print_barcode( string barcode,ostream& file){// prints the barcode.
	file << "Your Barcode : "<< endl;

	for (int i = 0 ; i < 2 ; i++){
		if (i == 0){
			for (int j = 0 ; j < 27 ; j++){
				if (barcode[j] == '1'){
					cout << "|";
					file << "|";
				}else{
					cout << " " ;
					file << " " ;
				}
			}
			cout << endl;
			file << endl;
		}
		else if(i == 1){
			for (int k = 0 ; k < 27 ; k++){
				cout << "|";
				file << "|";
			}
			cout << endl;
			file << endl;
		}
	}
	
}

void PostalService::findBarCode(int zipCode) {
	int digit;
	string barcode;
	int zipcode = zipCode;
	string file_name = to_string(zipcode);//creates a file in the name of zipcode.
	file_name +=".txt";
	ofstream file;
	file.open(file_name.c_str());
	if(file.fail()){
		cout <<"file not open";
		exit(1);
	}
	
	do {
		digit = zipCode % 10;//gives the trailing number.
		zipCode = zipCode / 10;
		switch (digit) {
			case 0: barCode = "11000" + barCode ;
					break;
			case 1: barCode = "00011" + barCode ;
					break;
			case 2: barCode = "00101" + barCode ;
					break;
			case 3: barCode = "00110" + barCode ;
					break;
			case 4:	barCode = "01001" + barCode ;
					break;
			case 5:	barCode	= "01010" + barCode ;
					break;
			case 6: barCode	= "01100" + barCode ;
					break;
			case 7: barCode	= "10001" + barCode ;
					break;
			case 8: barCode = "10010" + barCode ;
					break;
			case 9:	barCode	= "10100" + barCode ;
					break;
		}
	}while (zipCode > 0);
	barCode = "1" + barCode;//adds leading and tailing numbers.
	barCode = barCode + "1";
	print_barcode(barCode,file);
	cout << endl<<"Your zip code was saved in the file " << file_name<<endl;
	file.close();//make sure to close file


}


void PostalService::findZipCode(string barCode) {
	int zipCode = 0;
	string str,barcode="";
	barcode +=barCode;
	barcode =  "1" + barcode;//adds leading and trailing numbers.
	barcode = barcode + "1";
	str = barCode.substr(0, 5);//takes in 5 numbers at a time.
	if (str == "11000") {
		cout << "Error in the bar code string." << endl;
		exit(1);
	}
	do {
		str = barCode.substr(0, 5);
		barCode = barCode.substr(5);
		if (str == "11000")
			zipCode = zipCode * 10 + 0;
		else if (str == "00011")
			zipCode = zipCode * 10 + 1;
		else if (str == "00101")
			zipCode = zipCode * 10 + 2;
		else if (str == "00110")
			zipCode = zipCode * 10 + 3;
		else if (str == "01001")
			zipCode = zipCode * 10 + 4;
		else if (str == "01010")
			zipCode = zipCode * 10 + 5;
		else if (str == "01100")
			zipCode = zipCode * 10 + 6;
		else if (str ==	"10001")
			zipCode	= zipCode * 10 + 7;
		else if(str == "10010")
			zipCode	= zipCode * 10 + 8;
		else if(str == "10100")
			zipCode	= zipCode * 10 + 9;
		else
		{
			cout<<"Error in	the	specified barcode string."<<endl;
			exit(1);
		}
	}while (barCode.length() > 0);
	int zipcode = zipCode;
	string file_name = to_string(zipcode);
	file_name +=".txt";
	ofstream file;
	file.open(file_name.c_str());
	if(file.fail()){
		cout <<"file not open";
		exit(1);
	}

	cout << "Your zip code is "<< zipCode <<", and the bar code looks like this: "<< endl;
	print_barcode(barcode,file);
	cout <<endl <<"Your zip code was saved in the file: "<< file_name<<endl;
	barCode = to_string(zipCode);
	file.close();//make sure to close file


}
