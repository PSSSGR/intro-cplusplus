//FILENAME:		program7.cpp
//NAME:			Sri Padala
//WSUID:		u424p963
//HOMEWORK:		home work-7
//DESCRIPTION:	This program prompts the user to enter upto ten words and sorts them through bubble sort method. 



#include <iostream>
#include <string>

using namespace std;

void bubblesort(string words[],int num);//Sorts the words in an order through bubble sorting method.

const int NUM_WORDS = 10;//Maximum  number of words to be entered by the user. 

int main(void)
{

	string array_of_strings[NUM_WORDS];//To store words given by the user.
	string word;

	int count = 0;//Takes note of the number of words entered by the user.

	while( count < 10 )
	{
		cout << "enter a word : ";
		cin  >> word;
		if (word != "0")
		{
			array_of_strings[count]= word;
		}
		else if (word == "0")//if the user enters "0" the loop terminates.
		{
			break;
		}
		count++;
		cout << endl;
	}
	bubblesort(array_of_strings , count);//calling the bubble sort function.
	
	for(int k = 0;k < count;k++)//printing the words arranged in order.
	{
		cout << array_of_strings[k]<<" ";
	}

	return(0);
}

void bubblesort( string words[], int num)
{
	bool swapped = true;
	int j = 0;

	string tmp,tmp2;//Used to swap the words.
	string uppercase[num];//store the uppercase word.

	for (int a = 0; a < num; a++)//converts the given string to upper case and stores it.
	{
		for(int b = 0; (unsigned) b < (words[a].length());b++)
		{
			uppercase[a]+=toupper(words[a][b]);
		}
	}

	while (swapped)
	{
		swapped = false;
		j++;
		for ( int i=0; i< num - j;i++)
		{
			if (uppercase[i] > uppercase[i+1])//caparing the words.
			{
				tmp = uppercase[i];//compares the upper case word and sorts.
				uppercase[i] = uppercase[i+1];
				uppercase[i+1] = tmp;

				tmp2 = words[i];//sorts the normal words in the above sorted order.
				words [i] = words[i+1];
				words[i+1] = tmp2;

				swapped= true;
			}
		}
	}
}

	






	

