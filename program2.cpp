//FILENAME   :   program2
//NAME       :   SRI PADALA
//WSU ID     :   U424P963
//HW         :   HOMEWORK-2
//DESCRIPTION:   THIS PROGRAM SHOWS UP THE SHAPES OF AN INVERTED TRIANGLE,DIAMOND,AND X-SHAPE.USING "FOR LOOP" AND IF-ELSE LOOP.
#include <iostream>
#include <cmath>

using namespace std;

int main(void)

{
	//TRIANGLE
	int count = 8;//Initializing the variable count for the number of hashes.

	for ( int i=1 ; i<=4 ; i++ )
	{
		for ( int j=1 ; j<=count ;j++ )
		{
			cout << "#";
		}
		cout << "\n";
		count = count - 2;//As the number of hashes decreases from top to bottom by 2.we derease count by 2 for each iteration.
	
	    for ( int k=0 ; k<i ; k++ )
		    cout << " ";
	}
	//TRIANGLE

	cout << "\n\n";	

	
    //DIAMOND
	int e = 2;//Initializing the variable "e" for the number of hashes.
	for ( int a =-3 ; a<=3 ; a++ )
	{		
		for ( int b=0 ; b<abs(a) ; b++ )
		{
			cout << " ";
		}
		e = 8 - 2*abs(a);//As the number of hashes in relation to the row number follows this expression,we alter the value of e.
		for ( int c=0 ; c<e ; c++ )
		{
			cout << "#";
		}
		cout << endl;
	}
	//DIAMOND

    cout << "\n\n";
	
	
	//X-SHAPE
    for ( int i=1 ; i<=7 ; i++ )
	{
		for ( int j=1 ; j<=7 ; j++ )
		{
			if ( i == j || j == 8-i )//This relation tells about the relative position of the hashes with the corresponding row number.
				cout << "#";
			else 
				cout << " ";
		}
      	cout << endl;
	}
	//X-SHAPE
	return (0);
}

