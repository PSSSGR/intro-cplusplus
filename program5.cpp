//FILENAME		:PROGRAM5.
//NAME			:SRI PADALA.
//WSU ID		:U424P963.
//HOMEWORK		:HOMEWORK-5.
//DESCRIPTION	:THIS PROGRAM WILL PRINT A SONG USING ARRAYS AND FUNCTIONS.

#include <iostream>
using namespace std;

const int size=5;
//THIS FUNCTION WILL PRINT "BINGO" WHENEVER IT IS CALLED.
char print_bingo(const char arr[],int size);
const char dog[size]={'B','I','N','G','O'};

int main(void)
{
	for(int b=0;b<6;b++)//THIS WILL ITERATE THE LOOP SIX TIMES TO PRINT SIX PARAGRAPHS.
	{
		cout << "There was a farmer had a dog "<< endl;
		cout << "and ";
		cout << print_bingo(dog,size) << " was his name-o"<< endl;
		for(int k=0;k<3;k++)//THIS WILL ITERATE THE LOOP THREE TIMES TO PRINT THE WORD "BINGO".
		{
			for(int i=0;i<b;i++ )//THIS LOOP WILL PRINT "*" WITH RESPECT TO THE NUMBER OF PARAGRAPH.
			{
				cout << "*";
			}
			for (int j=b;j<=5;j++)//THIS LOOP WILL PRINT ARRAY WITH RESPECT TO THE NUMBER OF PARAGRAPH.
			{
				cout << dog[j];
			}
			cout<<endl;
		}
		cout << "And " ;
		cout << print_bingo(dog,size) <<" was his name-o"<<endl<<endl;
	}
	return(0);
}

char print_bingo(const char arr[], int size)
{
	for (int i=0;i<size;i++)
	{
		cout<<arr[i];
	}
	return(0);
}

