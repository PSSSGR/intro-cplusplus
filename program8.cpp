//Filename		: program8.cpp
//Name			: Sri Padala
//WSU ID		: u424p963
//HW			: Homw work-8
//Description	: This program will prompt the user to enter the number of wordshe/she will entering and sorts the words using bubble sort method.


#include <iostream>
#include <cctype>
#include <string>


using namespace std;

void bubblesort(string words[],int num);


int main(void)
{
	string *array_of_words;//Dynamic array to store words. 
	int num_of_words;//number of words given by user.
	string word;

	cout << "How many words will you be entering? : ";
	cin >> num_of_words;

	array_of_words = new string[num_of_words];//Setting up the the Dynamic Array.

	for(int i =0 ; i<num_of_words;i++)//Getting each word from user and storing it in array.
	{
		cout << "Enter a word : ";
		cin >>  word;
		*(array_of_words + i ) = word;
	}

	bubblesort(array_of_words, num_of_words);//calling the function.
	cout << "Sorted list : ";


	for (int k =0;k<num_of_words;k++)//Printing the sorted words.
	{
		cout << array_of_words[k] <<" ";
	}
	cout << endl;


	delete [] array_of_words;
	array_of_words = 0;

	return(0);
}

void bubblesort(string words[], int num)
{
	bool swapped = true;//Bollean statement.
	int j = 0;
	string tmp,tmp2;//To swap
	string uppercase[num];//To store uppercase word and sort.

	for(int k =0;k<num;k++)//Converts words into uppercase.
	{
		for(int e = 0;(unsigned)e< words[k].length();e++)
		{
			uppercase[k]+=toupper(words[k][e]);
		}
	}

	while(swapped)
	{
		swapped = false;
		j++;
		for ( int i =0 ; i<num-j;i++)
		{
			if(uppercase[i] > uppercase[i+1])
			{

				tmp = uppercase[i];//Swapping
				uppercase[i] = uppercase[i+1];
				uppercase[i+1] = tmp;

				tmp2 = words[i];//Swapping- sorts the words in the above sorted order.
				words[i] = words[i+1];
				words[i+1] = tmp2;


				swapped = true;
			}
		}
	}
}



