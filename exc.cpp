//Filename		: exc.cpp
//Name			: Sri Padala
//WSU ID		: u424p963
//Home work		: Home work (extra credit)
//Description	: This program takes in data from a file ,and encode and decodes it as per asked by the user. 

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <cstdlib>

using namespace std;

ifstream in_stream;//Declaring streams
ofstream out_stream;

void encode_rail_shift(string message,string encoded_message,int shift,int size,int rails);//Encodes the message.
void decode_rail_shift(string encoded_message,string decoded_message,int shift,int size,int rails);//Decodes the message. 


int main(int argc, char *argv[])//using command line arguements.
{

	int size = 0,rails = 2;//number of rails.
	int shift = atoi(argv[2]);//converts characters into intergers.

	string message,encoded_message,decoded_message;

	
	in_stream.open(argv[3]);//open the file.

	if(in_stream.fail())//checks if file is open or not.
	{
		cout << "Input file opening failed.\n";
		exit(1);
	}

	getline(in_stream,message);//reading mesage from file.
	size = message.length();

	for(int z =0; z<size;z++)//converts into upper case.
	{
		message[z]=toupper(message[z]);
	}
	

	if(strcmp( argv[1], "-e") == 0)//encode the message.
	{
		encode_rail_shift(message,encoded_message,shift,size,rails);//calling function.
		out_stream.open(argv[3]);//printing message to file.
		
		
		if (out_stream.fail())
		{
			cout <<"output file opening failed.\n";
			exit(1);
		}
		for (int b =0;b < size ;b++)
		{
			out_stream << encoded_message[b];
		}
		out_stream.close();
	}


	if(strcmp( argv[1], "-d") == 0)//decode the message.
	{
		decode_rail_shift(message,decoded_message,shift,size,rails);//calling function.
		out_stream.open(argv[3]);

		if (out_stream.fail())
		{
			cout <<"output file opening failed.\n";
			exit(1);
		}

		for (int b =0; b<size ;b++)//printing message to file.
		{
			out_stream << decoded_message[b];
		}
		out_stream.close();
	}
	in_stream.close();

	return(0);
}


void encode_rail_shift ( string message, string coded_message, int shift,int size,int rails)
{
	char letter;
	int k=0;//It represents the position of letter and store them in the array.
	
	
	for ( int i = 0 ; i < size ; i++)
	{
		letter = message[i];
		if ( letter >= 'A' && letter <= 'Z' )//Shifting
		{
			letter = ((letter + shift - 'A' + 26) % 26 + 'A');
			message[i] = letter;
		}
	}

	for (int  i = 1 ; i <= rails ; i++)//rail_fencer
	{
		if( i==1 || i==rails)
		{
			for (int  j=i-1 ; j<size ; j+=2*(rails-1))
			{
				coded_message[k++]=message[j];
			}
		}
	}

}

void decode_rail_shift(string encoded_message,string decoded_message,int shift,int size,int rails)
{
	int k=0;//They represent the position of letter and store them in the array.
	char letter;

	for (int  i=1 ; i <=  rails ; i++)//rail_fencer
	{
		if( i==1 || i== rails)
		{
			for ( int j=i-1; j<size ; j+=2*( rails-1))
			{
				decoded_message[j]=encoded_message[k++];
			}
		}
	}

	for ( int i = 0 ; i < size ; i++ )//shifting
	{
		letter = decoded_message[i];
		if ( letter >= 'A' && letter <= 'Z' )
		{
			letter = (( letter - shift - 'A' + 26) % 26 + 'A');
			decoded_message[i] = letter;
		}
	} 
	

}
