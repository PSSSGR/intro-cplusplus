//FILENAME    : PROGRAM3
//NAME        : SRI PADALA
//WSU ID      : U424P963
//HOMEWORK    : HOMEWORK-3
//DESCRIPTION : THIS PROGRAM WILL PROMPT THE USER TO ENTER HIS/HER BANK DETAILS(LOAN AMOUNT,INTEREST RATE,MONTHLY PAYMENT) AND CALCULATES THE INTEREST AND PAYMENT BALANCE IN A TABULAR FORM. 
#include <iostream>

using namespace std;

//Declaring funtion calculate_interest to calculate interest from loan amount and interest rate.
double calculate_interest( double loan_amount , double interest_rate )
{
	double result;
	result =( loan_amount * interest_rate ) / 1200 ;//Calculating the interest. 
	return result;
}
//Declaring function calculate_principal to calculate principal from monthly payment and interest. 
double calculate_principal( double temp_monthly_payment , double temp_interest )
{
	double result;
	result = ( temp_monthly_payment - temp_interest );
	return result;
}

//main function
int main(void)
{
	double loan,interest,interest_rate,monthly_payment,remaning_balance;

	//info from user

	cout << " Enter the loan amount :  ";
	cin  >> loan;
	cout << " Enter the interest rate : ";
	cin  >> interest_rate;
	cout << " Enter the monthly payment : ";
	cin  >> monthly_payment;

	//using loops for table

	for( int i=0 ; i<3 ; i++ )

		{
		cout << "\nYear " << i+1 << "\t\tInterest\tPrincipal\n" ;
		cout << "\t---" << "\t--------" << "\t---------\n" ;

		double total_interest=0,total_principal=0;//Initializing total_interest and total_principal to 0.

		for( int j=0 ; j<12 ; j++ )
			{
				interest = calculate_interest ( loan , interest_rate );//Calling the function calculate_interest. 
				remaning_balance = calculate_principal ( monthly_payment , interest );//Calling the function calculate_pricipal.
			  
				total_interest+= interest;//Iterating the variables for each time.
				total_principal+= remaning_balance;
				loan-= remaning_balance;
											
				cout.setf(ios::fixed);//For getting two decimal points.
				cout.setf(ios::showpoint);
				cout.precision(2);

				cout << "\t" << j+1 << ":\t" << interest << "\t\t" << remaning_balance << "\n";
																    
			}

		cout << "\t---" << "\t--------" << "\t---------\n";
		cout << "\nYear " << i+1 << "\tTotals: " << total_interest << "\t\t" << total_principal;
		cout << "\nRemaining Balance at year end : " << loan << "\n\n";
		}

	  return 0;
}
