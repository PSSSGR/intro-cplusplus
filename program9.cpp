//File Name   : program9.cpp 
//Name        : Sri Padala
//WSU ID      : u424p963
//HW          : HW-9
//Description : This program takes temperature data from file and gives information regarding max,min,and mean tempeature of the given data.

#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

void show_warmer(double temperature [][24], int days, double cutoff );//shows the warmer temperature to the given temperature.
double find_average(double temperature [][24], int days );//returns average temperature of the given data.
void find_daily_mmm(double day_temps[],double &max, double &min, double &mean );//finds max,min,mean of the given data.


ifstream in_file;//creating stream to retrive data from file.



const int days=7;


int main(void)
{
	double celsius[7][24];//To store data from file.
	double cutoff;//temperature entered by the user.
	double celsius_temp,avg_temp;

	cout << "Enter the value for which to find warmer temperatures (C):";
	cin  >> cutoff;

	in_file.open("temperature.dat");//open the file.
	

	if (in_file.fail())//checks if file is open or not.
	{
		cout << "Input file opening failed.\n";
		exit(1);
	}
	
	for (int i = 0 ; i < 7;i++){
		for (int j = 0 ; j < 24;j++){
			in_file >> celsius_temp; 
			celsius_temp = static_cast<double>(((celsius_temp - 32)*(5))/9); 
			celsius[i][j] = celsius_temp;
			}
		
				
	}

	show_warmer(celsius,days,cutoff);//calling th functions.
	cout << endl;
	avg_temp=find_average(celsius,days);

	cout << "The Average temperature over all the days was : "<<avg_temp <<"C."<<endl<<endl;
	double max=0,min=0,mean=0;


	cout << "The maximum, minimum, and  mean temperature for each day: "<<endl<<endl;
	for (int i=0;i<days;i++){
		find_daily_mmm(celsius[i], max,  min, mean );
		cout <<"Day  "<< i+1 << " : " << "max " << max << "C, "<<"min  "<<min<<"C,"<<"mean "<< mean << "C."<<endl;
		}
	




	in_file.close();//closing the files.
	



	return(0);
}

void show_warmer (double temperature [][24], int days , double cutoff)
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	
	cout << "Times at which temperatures warmer than " << cutoff <<" were found:"<<endl<<endl;

	for (int i =0;i<days;i++){//days count
		for (int j=0;j<24;j++){//hour count
			if ( cutoff < temperature [i][j]){
				cout.setf(ios::fixed);//sets the precision.
				cout.setf(ios::showpoint);
				cout.precision(2);
				cout << "At day "<<i+1<<","<<"hour "<<j+1<<","<<" the temperature was " << temperature [i][j]<<"C."<<endl;
			}
		}
	}
}

double find_average(double temperature [][24], int days){
	double average = 0;

	for (int i =0;i<days;i++){
		for (int j=0;j<24;j++){
			average += temperature[i][j];
		}
	}
	average = static_cast<double> ((average)/168);//7*24=168.
	return (average);
}


void find_daily_mmm(double day_temps[], double &max, double &min, double &mean){
	double max_temp,min_temp,mean_temp=0;

	max_temp = day_temps[0];
	min_temp = day_temps[0];



	for (int i=0;i<24;i++){

		if(day_temps [i] > max_temp){
			max_temp = day_temps[i];
		}

		if(day_temps [i] < min_temp){
			min_temp = day_temps[i];
		}
		mean_temp += day_temps[i];
	}

	mean_temp = static_cast<double> ((mean_temp)/24);

	max = max_temp;
	min = min_temp;
	mean = mean_temp;
}

	






















































