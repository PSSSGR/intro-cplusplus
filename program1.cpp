//Filename:    program1.
//Name:        Sri Padala.
//WSUID:       u424p963.
//HW:          Homework-1. 
//Description: This program will prompt for two integers and an operation and then display the result of applying the operation to the numbers.

#include <iostream>

using namespace std;

int main(void)

  {

	  int numb1,numb2,optnumb;
	  double result;

	  cout << "This program will prompt for two integers and an operation and then display the result of applying the operation to the numbers.\n" ;
      cout << "Enter the first integer: " ;
	  cin  >> numb1;
	  cout << "Enter the second integer: " ;
      cin  >> numb2;
	  cout << "The available operations are: \n" ;
	  cout << "1)Addition\n" ;
	  cout << "2)Subtraction\n" ;
	  cout << "3)Multiplication\n" ;
	  cout << "4)Division\n" ;
	  cout << "Enter the number for your choice of operation : " ;
	  cin  >> optnumb ;

	  if ( optnumb > 0 && optnumb < 5 ){
	  	  
	      if ( optnumb == 1 ){
	            result = numb1 + numb2 ;
	            cout << numb1 << "+" << numb2 << "=" << result << "\n" << endl ;

	      }else if ( optnumb == 2 ){
		        result = numb1 - numb2 ;
		        cout << numb1 << "-" << numb2 << "=" << result << "\n" << endl ;

	      }else if ( optnumb == 3 ){
		        result = numb1 * numb2 ;
		        cout << numb1 << "*" << numb2 << "=" << result << "\n" << endl ;

	      }else if ( optnumb == 4 ){

			  if ( numb2 != 0 ){
				result =(double) numb1 / numb2 ;//As we may get result in decimals we need cast to it in double. 
			    cout << numb1 << "/" << numb2 << "=" << result << "\n" << endl ;
			  }else{
				cout << numb1 << "/" << numb2 << " cannot be found because a number cannot be divided by zero.\n" << endl ;

	          }
	      }
	  }else{
	  
	      cout << optnumb << " is an invalid operation. Valid choices are 1,2,3,or4. Quitting Program.\n" << endl ;
	   }
	
	  return (0) ;
  }

