//FILENAME   : program6.cpp
//NAME       : Sri Padala
//WSU ID     : u424p963
//HOMEWORK   : home work - 6 
//DESCRIPTION: This program prompts the user to enter a message (200 words limit). Encodes and Decodes the given message with caeser shift of four and rail fence with two rails.


#include <iostream>
#include <cmath>
#include <cctype>

using namespace std;

int SIZE=200;//Size limit to the message entered from the user.
int SHIFT=4;//The number of positions a letter gets shifted in caeser shift.  
int RAILS=2;//Number of rails in rail fence shift. 
char letter;//To read the positions.


char encode_the_message(char message[],int size);
char decode_the_message(char message[],int size);
char encode_rail_shift(int size,char message[],char rail_fence[]);
char decode_rail_shift(int size,char rail_fence[],char decoded_message[]);

int main(void)
{
	char message[SIZE],rail_fence[SIZE],decoded_message[SIZE];
	int count=0;

	cout<<"Please enter a message (200 character limit): ";
	cin.get(letter);

	while(letter != '\n' && count < 200)
	{
		message[count] = toupper(letter);//Converts smaller case to upper case.
		cin.get(letter);
		count++;//Takes care of the lenght of an array.
	}
	cout << endl;


	cout << "Rail Fence Shift - " << endl << endl;
	cout << "Entered Message : ";

	for ( int i = 0 ; i < count ; i++)//Prints entered message.
	{
		cout<<message[i];
	}
	cout << endl;

	encode_the_message ( message , count );//Encodes the message - caeser shift.
	encode_rail_shift ( count , message , rail_fence );//Encodes the message - rail fence shift.
	cout << "Encoded Message : ";

	for ( int i = 0; i < count ; i++)//prints encoded message.
	{
		cout<<rail_fence[i];
	}
	cout<<endl;

	decode_rail_shift ( count , rail_fence , decoded_message );
	decode_the_message ( decoded_message, count );
	cout << "Decoded Message : " ;

	for( int i = 0;i < count; i++)//prints decoded message.
	{
		cout<<decoded_message[i];
	}
	cout << endl;
	cout << endl;

	return(0);
}


char encode_the_message ( char message[] , int size )
{
	for ( int i = 0 ; i < size ; i++)
	{
		letter = message[i];
		if ( letter >= 'A' && letter <= 'Z' )
		{
			letter = ((letter + SHIFT - 'A' + 26) % 26 + 'A');//It will shift the letter by 4.
			message[i] = letter;
		}
	}
	return(0);
}


char decode_the_message ( char message[] , int size)
{
	for ( int i = 0 ; i < size ; i++ )
	{
		letter = message[i];
		if ( letter >= 'A' && letter <= 'Z' )//It will shift the letter by 4.
		{
			letter = (( letter - SHIFT - 'A' + 26) % 26 + 'A');
			message[i] = letter;
		}
	}
	return (0);
}

char encode_rail_shift ( int size , char message[], char rail_fence[])
{
	int i=0,j=0,k=0;//They represent the position of letter and store them in the array. 

	for (  i = 1 ; i <= RAILS ; i++)
	{
		if( i==1 || i==RAILS)
		{
			for (  j=i-1 ; j<size ; j+=2*(RAILS-1))
			{
				rail_fence[k++]=message[j];
			}
		}
	}
	
	return(0);
}

char decode_rail_shift ( int size , char rail_fence[], char decoded_message[])
{
	int i=0,j=0,k=0;//They represent the position of letter and store them in the array. 

	for ( i=1 ; i <=  RAILS ; i++)
	{
		if( i==1 || i== RAILS)
		{
			for (  j=i-1; j<size ; j+=2*( RAILS-1))
			{
				decoded_message[j]=rail_fence[k++];
			}
		}
	}
	return(0);
}


	






















































